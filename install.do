#!/bin/sh
exec >&2
# phony

: ${PREFIX:=/usr/local}

SUDOAS=sudo
DOASUDO=doas
if [ x"$DESTDIR" = x ]; then
    which sudo >/dev/null && SUDOAS=sudoas
    which doas >/dev/null && DOASUDO=doasudo
    echo installing sudoas as $SUDOAS
    echo installing doasudo as $DOASUDO
fi

# install -m 0755 -D doasudo "$DESTDIR/$PREFIX/bin/$DOASUDO"
install -m 0755 -D sudoas "$DESTDIR/$PREFIX/bin/$SUDOAS"
