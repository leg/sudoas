#!/bin/sh
exec >&2
# phony

: ${PREFIX:=/usr/local}

set +e

if which sudoas >/dev/null; then
    rm "$DESTDIR/$PREFIX/bin/sudoas"
else
    if which sudo >/dev/null; then
	VERSION=$(sudo -? 2>/dev/null)
	[ "${VERSION%% *}" = sudoas ] &&
	    rm "$DESTDIR/$PREFIX/bin/sudo" ||
		echo sudo seems to be native: $SUDO
    else
	echo no sudoas to uninstall
    fi
fi

return

if which doasudo >/dev/null; then
    rm "$DESTDIR/$PREFIX/bin/doasudo"
else
    if which doas >/dev/null; then
	VERSION=$(doas -? 2>/dev/null)
	[ "${VERSON%% *}" = doasudo ] &&
            rm "$DESTDIR/$PREFIX/bin/doas" ||
		echo sudo seems to be native: $DOAS
    else
	echo no doasudo to uninstall
    fi
fi
